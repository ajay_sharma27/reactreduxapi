import firebase from 'firebase';
import "firebase/database";
var firebaseConfig = {
    apiKey: "AIzaSyCHgm1JdNg1zESX9Zvcr5u8o-muP6zPH28",
    authDomain: "todo-86eb2.firebaseapp.com",
    projectId: "todo-86eb2",
    storageBucket: "todo-86eb2.appspot.com",
    messagingSenderId: "463860997610",
    appId: "1:463860997610:web:dd0e0a962bd13229892693",
    measurementId: "G-5HYHQ3XYSM"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);
  export default firebase;
