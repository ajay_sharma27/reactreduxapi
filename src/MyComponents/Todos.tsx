import { TodoItem } from "./TodoItem";
import { useState, useEffect } from 'react';
import firebase from '../firebase';
export const Todos = (props: any) => {
    const [todoList, setTodoList] = useState<any>();
    useEffect(() => {
        const formRef = firebase.database().ref('List');
        formRef.on('value', (snapshot) => {
            const form = snapshot.val();
            const todoList = [];
            for (let id in form) {
                todoList.push({ id, ...form[id] });
            }
            setTodoList(todoList);
        });
    }, []);
    return (
        <>
            <div className="container " >
                <h3 >Todos List</h3>
                {todoList ? todoList.map((todo: any) => { return (<TodoItem todo={todo} onDelete={props.onDelete} onEdit={props.onEdit} />) }) : ''}
            </div>
        </>
    )
}
export default Todos;