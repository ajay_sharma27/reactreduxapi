import firebase from '../firebase'
import { useState } from 'react';
export default function Registration(props: { onTempAction: any }) {
    const [username, setUsername] = useState<string>('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const handleOnUsername = (e: any) => {
        setUsername(e.target.value);

    };
    const handleOnEmail = (e: any) => {

        setEmail(e.target.value);

    };
    const handleOnPassword = (e: any) => {

        setPassword(e.target.value);
    };
    const createForm = () => {
        const formref = firebase.database().ref('Registration');
        const form = {
            username,
            email,
            password
        };
        formref.push(form);
        props.onTempAction(1);
        alert('Details Registered');
    };

    
    const switchWindow = () => {
        props.onTempAction(1);
    }
    return (
        <>
            <div >
                <div className="p-5 mb-2 text-center ">
                    <p className="h1">Registration</p>
                    <form className="py-3">
                        <div className="row mb-3 justify-content-center align-items-center">
                            <label htmlFor="username" className="col-sm-1 col-form-label-lg "><p className="h5 ">Username:</p></label>
                            <div className="col-sm-4">
                                <input
                                    type="username" onChange={handleOnUsername} className="form-control "
                                    id="username" value={username}
                                />
                            </div>
                        </div>
                        <div className="row mb-3 justify-content-center align-items-center">
                            <label htmlFor="email" className="col-sm-1 col-form-label-lg "><p className="h5 ">Email:</p></label>
                            <div className="col-sm-4">
                                <input
                                    type="email" pattern=".+@globex.com" onChange={handleOnEmail} className="form-control "
                                    id="email" value={email} required
                                />
                            </div>
                        </div>
                        <div className="row mb-3 justify-content-center align-items-center">
                            <label htmlFor="password" className="col-sm-1 col-form-label-lg "><p className="h5">Password:</p></label>
                            <div className="col-sm-4">
                                <input
                                    type="password" onChange={handleOnPassword} className="form-control "
                                    id="password" value={password}
                                />
                            </div>
                        </div><br />

                        <div >
                            <button className="btn btn-success justify-content-center align-items-center" type="button" onClick={createForm}>Registration
                    </button>{' '}
                            <hr /><br />
                            <div><h4>If Already Registered, Click Here To {<button className="btn btn-outline-primary " type="button" onClick={switchWindow}>Login
                    </button>} </h4>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </>
    );
}

