import React from "react";
import { useState, useEffect } from 'react';

export default function MyForm() {
    const [counter, setCounter] = useState(0);
  
    useEffect(() => {
      const interval = setInterval(() => {
        setCounter(counter + 1);
      }, 1000);
  
      return () => {
        clearInterval(interval);
      };
    });
  
    return <h1>{counter}</h1>;
  };


