import firebase from '../firebase'
import { useState } from 'react';
import { addTodo } from '../redux/actions';
import { useDispatch } from 'react-redux';
export const AddTodo = (props: any) => {
    const [title, setTitle] = useState(props.currTitle);
    const [desc, setDesc] = useState(props.currDescription);
    let dispatch = useDispatch();
    const handleOnTitle = (e: any) => {
        setTitle(e.target.value);

    };

    const handleOnDesc = (e: any) => {

        setDesc(e.target.value);
    };

    const createList = () => {
        dispatch(addTodo({
            // id: uuid(),
            title: title,
            desc: desc
        }));
        setTitle('');
        setDesc('');
        const formRef = firebase.database().ref('List');
        const form = {
            title,
            desc
        };
        formRef.push(form)
    };

    return (
        <>
            <div className="container py-5">
                <h3>Add a Todo</h3>
                <form >
                    <div className="mb-3">
                        <label htmlFor="title" className="form-label">Todo Title</label>
                        <input type="text" onChange={handleOnTitle} value={title} className="form-control" id="title" aria-describedby="emailHelp" />
                    </div>
                    <div className="mb-3">
                        <label htmlFor="desc" className="form-label">Todo Desription</label>
                        <input type="text" onChange={handleOnDesc} value={desc} className="form-control" id="desc" />
                    </div>
                    <button className="btn btn-sm btn-success" type="button" onClick={createList}>Add Todo</button>
                </form>
            </div>
        </>
    )
}
export default AddTodo;
