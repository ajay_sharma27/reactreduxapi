import Login from "./Login";
import Registration from "./Registration";
import { useState } from 'react';
import TodoList from './TodoList'
export default function MyForm() {
    let mystyles = {
        minHeight: '100vh',
        backgroundImage: "url(/images/q5.jpg)",
        backgroundPosition: 'center',
        backgroundSize: `cover`,
        backgroundRepeat: 'no-repeat'
    }
    const [curr_comp, setCurrComp] = useState(0);
    return (
        <>
            <div className="form " style={mystyles}>
                {
                    curr_comp === 0 ?
                        <Registration onTempAction={setCurrComp} />
                        : (curr_comp === 1) ?
                            <Login onTempAction={setCurrComp} />
                            : <TodoList />
                }
            </div>
        </>
    )
}