import firebase from '../firebase'
import { useState } from 'react';


export default function Login(props : {onTempAction : any}) {

    const [username, setUsername] = useState<string>('');
    const [password, setPassword] = useState<any>('');
    const handleOnUsername = (e: any) => {
        setUsername(e.target.value);

    };

    const handleOnPassword = (e: any) => {

        setPassword(e.target.value);
    };
    const loginForm = () => {
        const formref = firebase.database().ref('Login');
        const form = {
            username,
            password
        };
        formref.push(form)
        props.onTempAction(2);
        alert('Log In Successfully');
    };

    const switchWindow = () =>{
        props.onTempAction(0);
    }
    
    return (
        <>
            <div >
                <div className="py-5 mb-10 text-center ">
                    <p className="h1">Login</p>
                    <form className="py-3" >
                        <div className="row mb-3 justify-content-center align-items-center">
                            <label htmlFor="username" className="col-sm-1 col-form-label-lg "><p className="h5 ">Username:</p></label>
                            <div className="col-sm-4">
                                <input
                                    type="username"  onChange={handleOnUsername} className="form-control "
                                    id="username" value={username}
                                />
                            </div>
                        </div>
                        <div className="row mb-3 justify-content-center align-items-center">
                            <label htmlFor="password" className="col-sm-1 col-form-label-lg "><p className="h5">Password:</p></label>
                            <div className="col-sm-4">
                                <input
                                    type="password" onChange={handleOnPassword} className="form-control "
                                    id="password" value={password}
                                />
                            </div>
                        </div><br />
                        <div >
                            <button className="btn btn-success justify-content-center align-items-center" type="button" onClick={loginForm}>Login</button><hr /><br />
                            
                            <div><h4>If Not An Existing User, Click Here To {<button className="btn btn-outline-danger " type="button" onClick={switchWindow}>Registration
                    </button>} </h4>
                    
                    </div>                           
                        </div>

                    </form>
                </div>
            </div>
        </>
    );
}


