import Header from "./Header";
import { Footer } from "./Footer";
import firebase from '../firebase';
import Todos from "./Todos";
import { AddTodo } from "./AddTodo";
import { useState } from 'react';
import { useDispatch } from 'react-redux';
import { onDelete, } from '../redux/actions';
export default function TodoList() {
  let dispatch = useDispatch();
  let myheader = {
    backgroundColor: '#e3f2fd'
  }
  let mystyle = {
    minHeight: '90vh',
    backgroundImage: "url(/images/q3.jpg)",
    backgroundPosition: 'center',
    backgroundSize: `cover`,
    backgroundRepeat: 'no-repeat'
  }

  const onDeleteLocal = (todo: any, props: any) => {
    dispatch(onDelete(todo.id));
    const formRef = firebase.database().ref('List').child(todo.id);
    formRef.remove();

  };

  const [todos, setTodos] = useState<any>([

  ]);

  let addTodo = (title: any, desc: any) => {
    let sno;
    if (todos.length === 0) {
      sno = 0;
    }
    else {
      sno = todos.length + 1;
    }
    const myTodo = {
      sno: sno,
      title: title,
      desc: desc,
    }
    setTodos([...todos, myTodo]);
  }

  return (
    <>
      <div className="header " style={myheader}>
        <Header title="Home" searchBar={false} />
      </div>
      <div className="list " style={mystyle}>
        <AddTodo addTodo={addTodo} />
        <Todos todos={todos} onDelete={onDeleteLocal} />
      </div>
      <Footer />
    </>
  );
}

